#include "../InDetGlobalTrackMonAlg.h"
#include "../InDetGlobalPrimaryVertexMonAlg.h"
#include "../InDetGlobalBeamSpotMonAlg.h"

DECLARE_COMPONENT( InDetGlobalTrackMonAlg )
DECLARE_COMPONENT( InDetGlobalPrimaryVertexMonAlg )
DECLARE_COMPONENT( InDetGlobalBeamSpotMonAlg )
